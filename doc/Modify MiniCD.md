Disk refurbishing utilities
===========================

In an attempt to create a new version of this tool, more compatible with the latest versions of Ubuntu, the following procedure was researched.


How to MODIFY a Ubuntu live CD created from scratch
---------------------------------------------------

See the instructions on [how to create a Ubuntu live CD from scratch](MiniCD.md) .

1. Repeat steps #2 and #3 .
2. You are now in step #6 and you can modify the ISO as needed.
3. Repeat steps #7, #10, #11, #13 and #14 .
