Disk refurbishing utilities
===========================

In an attempt to create a new version of this tool, more compatible with the latest versions of Ubuntu, the following procedure was researched.


How to create a Ubuntu live CD from scratch
-------------------------------------------

For the following procedure it is recommended that you work on the same version of Ubuntu as you want the live CD to be.

The procedure is based on:
https://help.ubuntu.com/community/LiveCDCustomizationFromScratch
...but certain procedures are excluded or updated to 18.04 .


1. Use `debootstrap` to install the base system into a folder

```bash
$ sudo apt-get install debootstrap
$ mkdir work
$ cd work
$ mkdir chroot

$ sudo debootstrap --arch=amd64 bionic chroot
```

2. Transfer certain key files and system structures

```bash
$ sudo mount --bind /dev chroot/dev
$ sudo cp /etc/hosts chroot/etc/
$ sudo cp /etc/resolv.conf chroot/etc/
$ sudo cp /etc/apt/sources.list chroot/etc/apt/
```

3. Go into the chroot and mount the other special filesystems

```bash
$ sudo chroot chroot
```
(the first chroot is the command, the second is the folder; at this point you are in the chroot and are working with a system that is a cross between the ISO and your own OS)

```bash
$ sudo mount none -t proc /proc
$ sudo mount none -t sysfs /sys
$ sudo mount none -t devpts /dev/pts

$ sudo export HOME=/root
$ sudo export LC_ALL=C

$ sudo dbus-uuidgen > /var/lib/dbus/machine-id
```

4. Install updates, so that the live CD contains the latest software

```bash
$ sudo apt-get update
$ sudo apt-get install aptitude vim

$ sudo aptitude full-upgrade
```

5. Install required system programs for the live CD

```bash
$ sudo apt-get install ubuntu-standard casper lupin-casper
$ sudo apt-get install discover laptop-detect os-prober
$ sudo apt-get install linux-generic
```

6. Install all other software and settings, as desired

(work in the terminal as if you are on the target OS)

7. clean up and prepare for ISO packaging

```bash
$ sudo rm /var/lib/dbus/machine-id

$ sudo apt-get autoremove
```
(this does nothing on my machine)

```bash
$ sudo apt-get clean
$ sudo apt-get autoclean

$ sudo rm -rf /tmp/*
$ sudo rm /etc/resolv.conf

$ sudo umount -lf /proc
$ sudo umount /sys
$ sudo umount /dev/pts

$ sudo exit
```
(this exits the chroot, you are once again in your OS)

```bash
$ sudo umount /dev
```

8. prepare the kernel and everything needed for the ISO

```bash
$ sudo apt-get install syslinux squashfs-tools genisoimage
```
(these were already installed for me)

```bash
$ sudo apt-get install isolinux

$ mkdir -p image/{casper,isolinux,install}

$ cp chroot/boot/vmlinuz-... image/casper/vmlinuz
$ cp chroot/boot/initrd-... image/casper/initrd.lz

$ cp /usr/lib/ISOLINUX/isolinux.bin image/isolinux/
$ cp /boot/memtest86+.bin image/install/memtest
$ cp /usr/lib/syslinux/modules/bios/ldlinux.c32 image/isolinux/
```

9. Create the boot menu

```bash
$ gedit image/isolinux/isolinux.cfg
```

Contents:

```
DEFAULT live
LABEL live
  menu label ^Start Ubuntu mini CD
  kernel /casper/vmlinuz
  append  file=/cdrom/preseed/ubuntu.seed boot=casper initrd=/casper/initrd.lz quiet splash text --
LABEL check
  menu label ^Check CD for defects
  kernel /casper/vmlinuz
  append  boot=casper integrity-check initrd=/casper/initrd.lz quiet splash --
LABEL memtest
  menu label ^Memory test
  kernel /install/memtest
  append -
LABEL hd
  menu label ^Boot from first hard disk
  localboot 0x80
  append -
TIMEOUT 300
PROMPT 0
```


10. Create the manifest file for Casper

```bash
$ sudo chroot chroot dpkg-query -W --showformat='${Package} ${Version}\n' | tee image/casper/filesystem.manifest

$ sudo cp image/casper/filesystem.manifest image/casper/filesystem.manifest-desktop
$ gedit image/casper/filesystem.manifest-desktop
```
Delete these entries:
ubiquity ubiquity-frontend-gtk ubiquity-frontend-kde casper lupin-casper live-initramfs user-setup discover1 xresprobe os-prober libdebian-installer4
(I am not certain if this is actually required, might only be needed if you are making an Ubuntu installer CD, which this isn't -- so don't worry if you mess it up)

11. Create squashfs

```bash
$ sudo mksquashfs chroot image/casper/filesystem.squashfs -e boot
```

12. Create diskdefines and ISO detection

(this is only required to help bootable USB making tools recognize your ISO)

```bash
$ gedit image/README.diskdefines
```

Contents:

```
#define DISKNAME  Ubuntu mini CD
#define TYPE  binary
#define TYPEbinary  1
#define ARCH  i386
#define ARCHi386  1
#define DISKNUM  1
#define DISKNUM1  1
#define TOTALNUM  0
#define TOTALNUM0  1
```

```bash
$ touch image/ubuntu
$ mkdir image/.disk

$ cd image/.disk
$ touch base_installable
$ echo "full_cd/single" > cd_type
$ echo "Ubuntu mini CD 18.04" > info

$ cd ..
$ cd ..
```

13. Create the hash files

```bash
$ cd image
$ find . -type f -print0 | xargs -0 md5sum | grep -v "\./md5sum.txt" > md5sum.txt
```

(...or probably `md5sum * > md5sum.txt` and then go into the file and delete the `md5sum.txt` row)

14. Generate the ISO file

```bash
$ mkisofs -r -V "Ubuntu mini CD" -cache-inodes -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o ../ubuntu-mini.iso .
```

(I got up to the file size 472 MB, it could probably go lower if you don't install as many tools)

15. Boot

Insert your ISO / CD and wait.

If you get a prompt that says "boot:", enter "live" and press Enter.

If you have any username and password prompts, the username is "ubuntu" and the password is blank. You can sudo from this user without a password.
