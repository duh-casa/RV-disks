# Shema platforme Diagnostik

Diagnostik je sestavljen iz treh sklopov, ki so medsebojno povezani da se zadeva uspešno naloži:

1. TFTP prenos boot menija (`pxelinux.0`), kar je binary ki naloži svojo konfiguracijo iz datoteke in prikaže meni ob zagonu, kjer lahko izbereš operacijski sistem ali nadaljuješ boot.
2. TFTP prenos Linux kernela (`vmlinuz`) z začetnim datotečnim sistemom (`initrd`)
3. NFS priklopna točka za živo inštalacijo Ubuntuja

Korak #1 ni nujno potreben, a je smiselen saj olajša nalaganje linux kernelov.

Korak #2 je verižno nalaganje in zagon Linux jedra. Jedra pridejo z fiksnim datotečnim sistemom vmlinuz, ki v bistvu vsebuje sestavne dele jedra -- razlog zakaj je tudi to datotečni sistem je pač zato ker je v Linuxu vse datotečni sistem.

Korak #3, takoj ko Ubuntu začne samostojno dihati, potrebuje pravi read-write datotečni sistem, da lahko naloži preostale programe. NFS je enostavno vrsta mrežnega datotečnega sistema ki je po eni strani dovolj nekompleksna da jo podpira kernel že v svoji osnovi brez posebnih programov (in le z NFS modulom), po drugi strani pa omogoča vse kar potrebuje POSIX datotečni sistem za svoje delovanje. Ko Ubuntu priklopi NFS v svoj root datotečni sistem, lahko začne nalagati navadne programe, pisane za sisteme ki tečejo z diska.

## Še tehnični update glede network boota (2018-10-13 03:41 "Poročilo")

Uspel sem konfigurirat `tftpd-hpa` in `nfs-kernel-server` programa da nudita vsebino CDROMa in omogočata network boot v live CD iz ISOta.

Ne bom posebej dokumentiral postopka, ker je moja implementacija zelo dirty. Ampak v bistvu ni drugega, kot to da odpakiraš ISO, nato pa narediš `pxelinux.0` konfiguracijo ki kot kernel naloži casper/vmlinuz in appenda boot opcije: `boot=casper netboot=nfs nfsroot=10.x.x.x:/srv/tftp initrd=casper/initrd`

Te boot opcije povejo da potem ko se prenese kernel, da se prenese še initrd (aka zagonski filesystem ki vsebuje driverje in podobno), za tem pa se zadeva preostanek ISO-ta naloži preko NFS, podobno kot sicer svoje datoteke Diagnostik.

Zadeva sicer še ne dela ustrezno, ker se zaplete v nekaj tekom boot-a, čeprav v bistvu potem ko greš v maintenence mode vse deluje ustrezno. Verjetno je problem v tem da nisem ustrezno navedel vseh boot opcij za 18-tko in bi moral narediti USB bootable ključek in nato preveriti kako izgleda pravilna konfiguracija. Možno je tudi da 18-tka ne vsebuje pomožnih skript, ki bi dale temu sistemu vedeti da se je vse ustrezno naložilo, kadar je boot preko mreže. Priponka naj bi nekaj povedala o tem.

Je pa tu večji problem, namreč TFTP protokol uporablja sistem po katerem mora prejemnik acknowledge-at vsakih 1416 bytov vsebine z paketkom da jih je uspešno prejel, kar v omrežju ki ima ne-ničen latency pomeni da traja cca 20 minut da se prenese vseh 8 MB kernela in 53 MB initrd-ja. Nisem prepričan zakaj tega problema nismo imeli v RSO že z Diagnostikom -- ali je res takšna razlika v latency-u, ker je moj strežnik 50m kabla stran v kleti? Pinga imam 0.2 ms. Ali pa je bil tisti kernel dejansko precej manjši od trenutnega.

Anyway, kot si lahko predstavljaš, 20minutni boot time zaradi prenosa dveh datotek po neučinkovitem protokolu je game-breaker. Sicer bi bilo mogoče možno uporabiti nekakšen vmesni linux (bootloader?), ki je majhen in bi se naložil preko TFTP hitro, nato pa bi preko nekega hitrejšega protokola prensel kernel in initramfs od Ubuntuja, ampak.... Glede na to da smo se že pogovarjali o opciji za Diagnostika, ki ne potrebuje poseganja v BIOS, bom trud raje usmeril v rešitev pri kateri kernel in initramfs naložimo preko USB ključka, sam OS pa se potem nalaga preko NFS, preko mreže.

Mislim da bi to moralo biti možno samo z igranjem z vnosi v loaderju USB ključa. Zadeva bi morala biti tudi raketno hitra, saj NFS dobro z RAMom založenega strežnika, kot veš je precej hitrejši od Flasha ali CD pogonov, obenem pa bi začetek boota bil mimo počasnega TFTP (za naložiti 61 MB iz USB ključa vseeno ni potrebno več kot sekundo), tako da sem optimističen. :)
