Disk refurbishing utilities
===========================

In an attempt to create a new version of this tool, more compatible with the latest versions of Ubuntu, the following idea was researched.

System transfer
---------------

This idea is a variant of the DOS "sys.com" program which transfers the current system to a new disk, but for modern Linux distributions (and is therefore much more elaborate).

It is a short version of some of the steps described in the [Progressive Linux installer](Progressive install CD.md) idea.