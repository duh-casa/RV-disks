Disk refurbishing utilities
===========================

In an attempt to create a new version of this tool, more compatible with the latest versions of Ubuntu, the following idea was researched.

Progressive install CD idea
---------------------------

The idea documented in this file is to create a live Ubuntu CD, which can install itself to a harddisk in the machine progressivelly, allowing the operator to test out the machine while it is installing and loose minimal time setting it up.

The CD or DVD would be created using the procedure documented in [How to create a Ubuntu live CD from scratch](MiniCD.md) .

The CD would boot up to a desktop interface, with an Install icon. The Install icon links to a master shellscript, which runs the individual sub-scripts.

Each sub-script verifies whether it requires to run or not, depending on the presence of key files in the harddisk filesystem (if any). These key files are created by the end of the previous sub-script, allowing the code to automatically detect which step if any, needs to be done next, even if the operator has restarted the machine mid-install.

The sub-scripts would preform as follows:
1. Zero the first and last 512 bytes of the harddisk using `dd` .
```
dd if=/dev/zero of=/dev/sda bs=512 count=1024
dd if=/dev/zero of=/dev/sda bs=512 seek=$(( $(blockdev --getsz /dev/sda) - 1024 )) count=1024

partprobe
```
2. Partition the harddisk using `parted` and `mkfs`, to create the boot `ext4` system, covering 90% of the disk and an install `ext4` partition covering the last 10%
```
parted -s -a optimal -- /dev/sda mklabel msdos
parted -s -a optimal -- /dev/sda mkpart primary ext4 '0%' '90%'
parted -s -a optimal -- /dev/sda mkpart primary ext4 '90%' '100%'

dd if=/dev/zero of=/dev/sda1 bs=512 count=1024
dd if=/dev/zero of=/dev/sda2 bs=512 count=1024

mkfs.ext4 /dev/sda1
mkfs.ext4 /dev/sda2

e2label /dev/sda1 boot
e2label /dev/sda2 install
```
3. `rsync` the live system into the boot partition
```
mkdir -p /mnt/boot
mount /dev/sda1 /mnt/boot
rsync -axAX --info=progress2 --exclude='/swapfile' / /mnt/boot/
```

At this point move `/etc/initramfs-tools/hooks/overlay` and `/etc/initramfs-tools/scripts/init-bottom/overlay` from [OverlayFS for Progressive Linux Installer](https://gitlab.com/duh-casa/overlayfs) into place. You can use the  installer to avoid mistakes:
```
git clone https://gitlab.com/duh-casa/overlayfs.git
overlayfs/install.sh
```

4. Set the root filesystem to be an `overlayfs`, combining the boot and install partitions, with the boot partition being readonly and the install partition being read-write

```
mkdir /mnt/boot/rofs /mnt/boot/cow /mnt/install
mount /dev/sda2 /mnt/install

mkdir /mnt/install/rw /mnt/install/work

mount --bind /dev /mnt/boot/dev

chroot /mnt/boot /bin/bash

mount tmpfs -t tmpfs /tmp
mount none -t proc /proc
mount none -t sysfs /sys
mount none -t devpts /dev/pts
```

Update initramfs for the overlay.

```
update-initramfs -u
```

5. Make the harddisk bootable (`grub` install)

Probably:
```
update-grub
/usr/sbin/grub-install --recheck --no-floppy /dev/sda
sync
```

At this point the system can boot up without the install CD. A reboot should be required at this step, otherwise zerofree will not work.
 
6. Run `zerofree` on the boot partition, which at this point is mounted readonly
```
apt install zerofree
zerofree -v /dev/sda1
```

7. Remount the boot partiton as readwrite and `rsync` the files from the install partition into the boot partition

This is essentially [synchronise.sh from the overlay project](https://gitlab.com/duh-casa/overlayfs/blob/master/synchronise.sh).

8. Remove the `overlayfs` and remount to the boot partition alone
```
rm /etc/initramfs-tools/hooks/overlay
rm /etc/initramfs-tools/scripts/init-bottom/overlay
update-initramfs -u
./synchronise.sh
reboot
```

At this point the installation has significantly more free disk space.

9. Repartition the install partition as swap
```
e2label /dev/sda2 swap
mkswap /dev/sda2
```

10. Run `sswap` on the swap partition
```
sswap -f -l -l -z /dev/sda2
```

11. Mount the swap partition
```
swapon /dev/sda2
```

At this point the installation has swap space.

12. Remove the install files

The procedure is designed so that each step is either short, or restartable. The installation securely erases the disk it is installed on and can complete this procedure when the computer is no longer handled by the operator, and is already being used by the end-user, with the usability of the machine growing progressively better as the install completes. 

When the procedure is complete, the system functions as a normal Ubuntu install.

Fast progressive install
------------------------

This is an alternate idea which is shorter and perhaps more time-efficient.



The sub-scripts would preform as follows:
1. Zero the first and last 512 bytes of the harddisk using `dd` .
```
dd if=/dev/zero of=/dev/sda bs=512 count=1024
dd if=/dev/zero of=/dev/sda bs=512 seek=$(( $(blockdev --getsz /dev/sda) - 1024 )) count=1024

partprobe
```

2. Partition the harddisk using `parted` and `mkfs`, to create the boot `ext4` system, covering 10GB of the disk and an unformatted partition covering the remaining space.
```
parted -s -a optimal -- /dev/sda mklabel msdos
parted -s -a optimal -- /dev/sda mkpart primary ext4 '0%' '10GB'
parted -s -a optimal -- /dev/sda mkpart primary free '10GB' '100%'

dd if=/dev/zero of=/dev/sda1 bs=512 count=1024
dd if=/dev/zero of=/dev/sda2 bs=512 count=1024

mkfs.ext4 /dev/sda1

e2label /dev/sda1 boot
```

3. Mount the boot partition and rsync the system to it:
```
mkdir -p /mnt/boot /mnt/nfs

mount /dev/sda1 /mnt/boot
mount -t nfs 192.168.0.2:/nfs/install2 /mnt/nfs

rsync -axAX --info=progress2 /mnt/nfs/ /mnt/boot/
```

An idea for doing this with multiple disks simultaneously would be to run `rsync` to a cache directory alongside with looping `rsync`s to each individual disk. When the main `rsync` process finishes, do one more run with each of the other ones.

4. Also copy the proins installer:
```
mkdir -p /mnt/boot/proins
rsync -axAX /proins/ /mnt/boot/proins/
```

Do not forget to also create any proins stage status indicators on the target disk before unmount.

5. Install GRUB and ddjust disk UUIDs:

The install2 UUID (a) is always the same. Use `lsblk -no UUID /dev/sda1` to get the disk UUID (b). 

```
grub-install --force --boot-directory=/mnt/boot/boot /dev/sda
sed -i 's/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb/g' /mnt/boot/boot/grub/grub.cfg
sed -i 's/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb/g' /mnt/boot/etc/fstab
```

6. Unmount partition and zerofree
```
umount /dev/sda1
zerofree -v /dev/sda1
sync
```

At this point, a reboot should likely follow. Or if you are doing multiple disks at the same time, install the disks into the target machines and proceed from there.

7. Erase the second partition.
```
ddrescue --force /dev/zero /dev/sda2 /proins/vars/erase.log
```

The write error at the end can safely be ignored (`||:`).

8. Remove the empty partition and extend the ext4 one:
```
parted -s -- /dev/sda rm 2
```

`parted -s -a optimal -- /dev/sda resizepart 1 '100%'` does not work due to https://bugs.launchpad.net/ubuntu/+source/parted/+bug/1270203 . Slightly hazardous alternative:
```
printf "d\nn\np\n1\n\n\nn\np\nw\n" | fdisk -B /dev/sda
partprobe
```

9. Enlarge filesystem:
```
resize2fs /dev/sda1
touch /forcefsck
sync
```

The `touch /forcefsck` is optional, but probably a good idea. Disk will be checked on next boot.

10. Create and add swap:
```
fallocate -l 4G /swapfile
chmod 600 /swapfile
mkswap /swapfile
echo "/swapfile      none            swap    sw              0       0">>/etc/fstab
swapon -a
```

At this point, no reboot is necessary.















