Disk refurbishing utilities
===========================

In an attempt to create a new version of this tool, more compatible with the latest versions of Ubuntu, the following idea was researched.

# NFS boot Ubuntu idea

The idea documented in this file is to create a USB key which can boot Ubuntu 20.04 from a NFS share, allowing a standard installer to be distributed conventiently without requiring special technical knowledge by the operator.

All of these steps prefixed with # should be executed using root, either by `sudo` or directly.

## Server setup

These steps are to be taken on the NFS server:

1. Install `nfs-kernel-server`:
```
# apt install nfs-kernel-server
```

2. Edit `/etc/export` (note no space before bracket):
```
/data/share 192.168.0.0/24(rw,nohide,no_root_squash,insecure,no_subtree_check,async)
```

3. Restart the NFS server:
```
# systemctl restart nfs-kernel-server
```

## Client setup

### NFS root filesystem

1. Install Ubuntu 20.04 onto a computer with a harddisk.

2. Install `nfs-client`:
```
# apt install nfs-client
```

3. Mount the NFS share (note `/data/share` is the absolute path on the server):
```
# mkdir -p /media/nfs
# mount -t nfs 192.168.0.2:/data/share /media/nfs
```

This will allow the files to be transfered to NFS later.

4. Create `/etc/grub.d/09-nfs` from [09-nfs](../proins/etc/grub.d/09_nfs)

This file is a slightly modified `10-linux` script, that adds a NFS boot option at the GRUB menu. The notable changes are the removal of the non-optional "ro" boot tag and providing the FS root as NFS rather than the disk UUID.

You might also want to remove `quiet splash` from `/etc/default/grub` to simplify debugging.

5. Generate GRUB configuration:
```
# update-grub2
```

6. Edit `/etc/initramfs-tools/modules` and add:
```
nfsv3
nfsv4
nfs_acl
```

7. Generate initramfs configuration:
```
# update-initramfs -u
```

8. Edit `/etc/NetworkManager/NetworkManager.conf` and add:
```
[keyfile]
unmanaged-devices=*
```

This is required because NetworkManager will deconfigure the network interface during boot, breaking the NFS connection holding the root filesystem and resulting in a system freeze. As NFS will be booted on different machines, we cannot define the interface by MAC address or anything like that.

Your network connection will already be configured by init scripts, so you don't need NetworkManager.

9. Edit `/etc/fstab` and comment out all the other entries, and add:
```
192.168.0.2:/data/share / nfs rw 0        0
```

Commenting out the other things is required to prevent the machine from trying to mount the harddisk which might not be present during boot. Furthermore a swap file on NFS makes little sense. 

Adding the line for the NFS mount is not required, as NFS will be mounted by init scripts whether anything is defined here or not, but the entry may help programs understand where the root partition is.

10. Copy the configured system to NFS:
```
# rsync -axAXv / /media/nfs/ --exclude='/media/nfs'
```

At this point it's recommended that you reboot the machine to see if your NFS configuration works. GRUB the kernel and initramfs will be loaded off the disk, then it should pick up the network.

If you make additional changes beyond this point, do not forget to repeat `rsync` to put your changes onto the NFS share.

### USB boot

1. Create an `ext4` partition on your USB key

You need about 300 MB, but 2 GB might be a nice safety margin.

2. Mount your USB key and create the `/boot` directory on it:
```
# mkdir /media/USB/boot
```

3. Install GRUB to your USB key:
```
# grub-install --force --removable --boot-directory=/media/USB/boot /dev/sdb
```

4. Copy your boot files to the USB key:
```
# rsync -axAXv /boot/ /media/USB/boot/
```

5. Adjust the UUIDs in the GRUB configuration:
```
# sed -i 's/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb/g' /media/USB/boot/grub/grub.cfg
```

You are trying to replace the boot Disk UUID (a) with the USB UUID (b). Use these to determine them:
```
# lsblk -no UUID /dev/sda
# lsblk -no UUID /dev/sdb
```

Doing this using `update-grub2` does not work because the script is hardwired to use the UUID of the system disk and will not accept the USB key.

6. Safely remove the USB key and you may reboot

Because your boot files were previously configured to use NFS, the USB key will only be used to load GRUB, the kernel and initramfs, then it will switch over to NFS. This means that you can remove the harddisk, or boot the USB key in a different machine and it will work.

### RAM disk Overlay

If you are booting multiple machines simultaneously, it is recommended to configure a RAM disk overlay. With this, NFS will be read-only and all writes will be stored in RAM locally. This prevents writes from different machines, sharing a single disk from creating unforseen consequences.

1. Create `/etc/initramfs-tools/hooks/overlay` and add the following content:
```
#!/bin/sh
PREREQ=""
prereqs()
{
    echo "$PREREQ"
}

case $1 in
    prereqs)
        prereqs
        exit 0
        ;;
esac

. /usr/share/initramfs-tools/hook-functions

manual_add_modules overlay

exit 0
```

2. Create `/etc/initramfs-tools/scripts/nfs-bottom/overlay` and add the following content:
```
#!/bin/sh
PREREQ=""
prereqs()
{
    echo "$PREREQ"
}

case $1 in
    prereqs)
        prereqs
        exit 0
        ;;
esac

mkdir -p /rofs /cow
mount -n -o move "${rootmnt}" /rofs
mount -t tmpfs -o size=50% tmpfs /cow
mkdir -p /cow/rw /cow/work
mount -t overlay -o lowerdir=/rofs,upperdir=/cow/rw,workdir=/cow/work root "${rootmnt}"
mkdir -p "${rootmnt}/rofs" "${rootmnt}/cow"
mount -n -o move /rofs "${rootmnt}/rofs"
mount -n -o move /cow "${rootmnt}/cow"

exit 0
```

3. Make both files executable:
```
chmod a+x /etc/initramfs-tools/hooks/overlay
chmod a+x /etc/initramfs-tools/scripts/nfs-bottom/overlay
```

4. Compile initramfs:
```
update-initramfs -u
```

5. It is recommended that you comment out all lines in `/etc/fstab` to prevent accidential manual root remount which breaks the system.

6. Repeat steps #4 and #5 of the above "USB boot" section, if you used it.

7. Reboot

After reboot, changes will no longer be saved to NFS. You can still make changes persistent by mounting it a second time and rsyncing your live root with it.
