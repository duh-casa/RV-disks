Prenos iz Google Drive

# Log vnosi na temo postavitve diagnostika

Diagnostik se je postavljal 2x… enkrat na skladišču in enkrat na vezje strežniku. Ob postavitvi smo pisali log vnose, iz katerih se da razbrati potrebne korake za postavitev sistema.

## Skladišče

### 2014-08-24
Subject: Namestitev tFTP strežnika.

Namen: Imeti tFTP strežnik za BOOTp (PXE) bootanje diagnostičnih programov.

Postopek:

* Navodila: http://askubuntu.com/questions/201505/how-do-i-install-and-run-a-tftp-server
```
# aptitude install xinetd tftpd tftp

# vim /etc/xinetd.d/tftp
```
(nov file)

Vpisal vsebino:
```
service tftp
{
protocol        = udp
port            = 69
socket_type     = dgram
wait            = yes
user            = nobody
server          = /usr/sbin/in.tftpd
server_args     = /home/slike
disable         = no
}
```

```
# service xinetd restart
```

Test delovanja sledi naslednjič.



### 2014-08-30
Subject: Priprava prvega testa network boota.

Namen: Imeti platformo za pripravo diagnostičnega programa, ki se samodejno naloži preko mreže.

Postopek:

* https://help.ubuntu.com/community/DisklessUbuntuHowto
* zaradi enostavnosti smo `nfs-kernel-server` in tftpd server odstranili (aptitude remove) in namesto njega namestili `tftpd-hpa`
* nekatere mape je potrebno ustvariti

```
# vim /etc/default/tftpd-hpa
```

```
TFTP_USERNAME="tftp"
TFTP_DIRECTORY="/home/slike/tftpboot"
TFTP_ADDRESS="0.0.0.0:69"
TFTP_OPTIONS="-vvv --secure"
```

```
# service tftpd-hpa restart
```

Smoothwall nastavitve:

* Network Boot enabled: kljukica
* Boot server: `192.168.0.59`
* Boot filename: `pxelinux.0`
* Root path: `/`

```
# cp /boot/memtest86+.bin /home/slike/tftpboot/memtest
```
(note da je filename drug, ker tftpd dela nekaj čudnega z končnicami: http://www.thenoccave.com/2012/03/28/pxe-boot-memtest/ )

```
# aptitude install syslinux
# cp /usr/lib/syslinux/pxelinux.0 /home/slike/tftpboot/

# vim /home/slike/tftpboot/pxelinux.cfg/default
```

```
SAY Nalagam diagnosticni program Duh casa...

DEFAULT Diagnostik

LABEL Diagnostik
  KERNEL memtest
```

* Test delovanja uspešen.
* Naslednji korak priprava Ubuntu server (12.04 32-bit obstoječi CD) inštalacije, prenos v NFS in zagon linux kernela z NFS opcijami preko syslinuxa.

### 2014-08-31-a
Subject: Delo na network bootu z linux kernelom.


Namen: Imeti platformo za pripravo diagnostičnega programa, ki se samodejno naloži preko mreže.
P.S.: Ker smo delali vse živo ta log ne vsebuje vsega potrebnega za priti do sedanjega stanja. Moral pa bi vsebovati vse kar je smiselno in pomembno.

* Ugotovili da je `nfs-kernel-server` v bisvtu NFS server za filesystem in ga namestil nazaj.

```
# vim /etc/exports
```

```
/home/slike/diagnostik 192.168.0.0/24(rw,nohide,no_root_squash,insecure,no_subtree_check,async)
```

* Namestili ubuntu server na fizični računalnik

```
# vim /etc/initramfs-tools/initramfs.conf (na fizičnem računalniku)
```

```
MODULES=netboot
BOOT=nfs
```

```
# update-initramfs -u
```
* Nato preneseš initrd, spremeniš nazaj in ponovno update, da se mašina lahko zboota z diska.

```
# aptitude install nfs-client
```

* Z live-cd prenesli vsebino na `/home/slike/diagnostik/original` (in ga skopiral v mapo `test1`), preko `sshfs`
```
# cp -r --preserve=all . /media/sshfsmnt
```

```
# vim /home/slike/tftpdboot/syslinux.cfg/default (na boot serverju)
```

```
DEFAULT test1

LABEL test1
  KERNEL vmlinuz
  APPEND root=/dev/nfs initrd=initrd nfsroot=192.168.0.59:/home/slike/diagnostik/test1 ip=dhcp rw
```

* initrd in vmlinuz skopiraš direktno v `/home/slike/tftpdboot` (na boot serverju)


TODO:

  * v tej točki kernel pravi mount: protocol not supported, po vsem researchu sodeč to pomeni da ustrezna podpora za NFS ni compiled-in
  * https://help.ubuntu.com/community/Desktop/PXE
  * http://www.howtogeek.com/howto/ubuntu/how-to-customize-your-ubuntu-kernel/


### 2014-08-31-b
Appendix za zadnji modification:
* https://bugs.launchpad.net/ubuntu/+source/initramfs-tools/+bug/1217041

```
# vim /etc/initramfs-tools/modules (na fizičnem računalniku)
```

```
nfsv3
nfsv4
nfs_acl
```

```
# update-initramfs -u
```
* Nato preneseš initrd, spremeniš nazaj (`BOOT=local` in `MODULES=most`) in ponovno update, da se mašina lahko zboota z diska.

* sedaj mounta normalno, ampak se potem zatakne.

### 2014-09-13-a
Nadaljno delo na PXE bootu:

* datoteke za diagnostik je bilo treba chownat na isti uid kot na remote mašini (0 za root in 1001 za test).

TODO za naslednjič:

```
Sep 13 06:22:44 diagnostik (192.168.0.75) dhclient:  DHCPREQUEST of 192.168.0.83 on eth0 to 255.255.255.255 port 67
Sep 13 06:22:44 diagnostik (192.168.0.75) dhclient:  DHCPNAK from 192.168.0.1
```

To se verjetno zgodi, ker zadeva pride na mrežo in misli da je druga mašina in hoče svoj IP nazaj. Zgodi se DHCP NAK, zadeva zgubi IP in s tem tudi NFS -- in zmrz.


### 2014-09-13-b
Potem sem razmišljall o tej napaki in... pač tisti moment ko prideš domov, se spraviš v pojslo in nenadoma veš rešitev.

Tista stvar ki jo moramo izklopit je DHCP client. V enem od postov je pisalo da je to treba ampak jaz sem seveda mislil da bom rešil z statičnim IP naslovom. Ne, DHCP strežnik je tisti ki nastavi stari IP na vmesnik.

https://help.ubuntu.com/community/DisklessUbuntuHowto
```
#iface eth0 inet dhcp
iface eth0 inet manual
```
Poleg tega bo, odvisno katere buge so že odpravili, morda treba odinštalirati tudi Network Manager. ;)

### 2015-01-10
Datoteke Diagnostika sem ponesreči zavozil, zato smo pripravili nove v:
```
/home/diagnostik/files
```
...tftp mapico pa sem prestavil v:
```
/home/diagnostik/core
```

S tem je diagnostik sedaj v celoti prestavljen iz Samba shareov v samostojno mapico.

### 2015-01-17
Odpravil napako zaradi katere Diagnostik ni deloval. Problem je v tem da če je vklopljen DHCP, potem mašina zahteva nov IP potem ko je NFS že povezan, kar povzroči da se NFS prekine, mašina izgubi dostop do svojega diska in se obesi.

V `/etc/network/interfaces` je potrebno zakomentirati vrstico z DHCP in dopisati:
```
iface eth0 inet manual
nameserver 10.9.8.1
```

Sedaj diagnostik dela. Bom vnesel isti popravek še v nastavitve vezje strežnika.


## Vezje

### 2014-11-26
Danes sem namestil še `nfs-kernel-server` in `tftp-hpa`. V `/etc/exports` sem dodal ustrezno vrstico:
```
/sync/slike/diagnostik 10.9.7.0/24(rw,nohide,no_root_squash,insecure,no_subtree_check,async)
```
in:
```
# service nfs-kernel-server restart
```

Popravil pot v `/etc/default/tftpd-hpa`

Ugotovil sem da bo treba mapo diagnostika in tftpboot prestaviti iz mape slike, saj tega ne moremo sinhronizirati, ker so različna omrežja.

Sedaj sem preveč zaspan da bi na tem delal, tako da bom ugasnil server in nadaljeval naslednjič.

### 2015-01-10
Prenesel sem nastavitve in datoteke za Diagnostika na vezije v `/home/diagnostik` enako kot na skladišču.



